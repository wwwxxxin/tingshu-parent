package com.atguigu.tingshu.vo.album;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "专辑统计信息")
public class AlbumStatVo {

    @Schema(description = "专辑id")
    private Long albumId;

    @Schema(description = "播放量")
    private Long playStatNum;

    @Schema(description = "订阅量")
    private Long subscribeStatNum;

    @Schema(description = "购买量")
    private Long buyStatNum;

    @Schema(description = "评论数")
    private Long commentStatNum;
}
