package com.atguigu.tingshu.vo.search;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class AlbumSearchResponseVo implements Serializable {

    private static final long serialVersionUID = 1L;

    // 检索出来的商品信息
    private List<AlbumInfoIndexVo> list = new ArrayList<>();

    // 总记录数
    private Long total;

    // 每页显示的内容
    private Integer pageSize;

    // 当前页面
    private Integer pageNo;

    // 总页码
    private Long totalPages;

}
