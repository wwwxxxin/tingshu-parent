package com.atguigu.tingshu.model.user;

import com.atguigu.tingshu.model.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/***
 * 自定义 用户收藏表 数据库操作
 */
@Data
@TableName("user_collect")
public class UserCollectTable extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@TableField("user_id")
	private Long userId;

	@TableField("track_id")
	private Long trackId;
}