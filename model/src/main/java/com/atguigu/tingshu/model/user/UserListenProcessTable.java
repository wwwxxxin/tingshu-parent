package com.atguigu.tingshu.model.user;

import com.atguigu.tingshu.model.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

/***
 * 自定义 用户播放进度表 数据库操作
 */
@Data
@TableName("user_listen_process")
public class UserListenProcessTable extends BaseEntity {

	@TableField("user_id")
	private Long userId;

	@TableField("album_id")
	private Long albumId;

	@TableField("track_id")
	private Long trackId;

	@TableField("break_second")
	private BigDecimal breakSecond;

}