package com.atguigu.tingshu.album.client;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.tingshu.album.service.TrackInfoService;
import com.atguigu.tingshu.model.album.TrackInfo;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * 声音 内部调用接口
 */
@RestController
@AllArgsConstructor
@RequestMapping(value = "/client/track/trackInfo")
public class TrackClientController {

    private TrackInfoService trackInfoService;

    /**
     * 查询声音是否存在
     *
     * @param trackId 声音id
     * @return 是否存在
     */
    @GetMapping(value = "/getTrackInfo/{trackId}")
    public Boolean getTrackInfo(@PathVariable(value = "trackId") Long trackId) {
        return trackInfoService.getById(trackId) != null;
    }

    /**
     * 查询即将订单确认的声音信息
     *
     * @param trackId 声音id
     * @return 声音信息
     */
    @GetMapping(value = "/getTrackListAndPrice/{trackId}")
    public JSONObject getTrackListAndPrice(@PathVariable(value = "trackId") Long trackId) {
        return trackInfoService.getTrackListAndPrice(trackId);
    }

    /**
     * 查询声音详情信息
     *
     * @param trackId 声音id
     * @return 声音信息
     */
    @GetMapping(value = "/getTrackInfoDetail/{trackId}")
    public TrackInfo getTrackInfoDetail(@PathVariable(value = "trackId") Long trackId) {
        return trackInfoService.getById(trackId);
    }
}