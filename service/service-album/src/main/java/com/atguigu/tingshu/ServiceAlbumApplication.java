package com.atguigu.tingshu;

import lombok.AllArgsConstructor;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RedissonClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@AllArgsConstructor
@EnableScheduling      // 开启定时任务
@EnableFeignClients    // 开启feign客户端
@EnableDiscoveryClient // 开启服务注册与发现
@SpringBootApplication
public class ServiceAlbumApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceAlbumApplication.class, args);
    }

    private RedissonClient redissonClient;

    /**
     * 初始化布隆过滤器
     *
     * @return
     */
    @Bean
    public RBloomFilter initBloomFilter() {
        // 初始化布隆过滤器
        RBloomFilter<Object> albumBloomFilter = redissonClient.getBloomFilter("album_bloom_filter");
        // 初始化布隆过滤器的预计元素为200000L，期望误差率为0.01
        albumBloomFilter.tryInit(200000L, 0.01);

        return albumBloomFilter;
    }
}