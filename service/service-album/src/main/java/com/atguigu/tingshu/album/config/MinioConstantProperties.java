package com.atguigu.tingshu.album.config;

import io.minio.MinioClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "minio") // 读取节点
@Data
public class MinioConstantProperties {

    private String endpointUrl;
    private String accessKey;
    private String secreKey;
    private String bucketName;

    /**
     * minio客户端对象初始化
     *
     * @return minio客户端对象
     */
    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder()
                // 设置minio服务地址
                .endpoint(endpointUrl)
                // 设置认证信息
                .credentials(accessKey, secreKey)
                .build();
    }
}
