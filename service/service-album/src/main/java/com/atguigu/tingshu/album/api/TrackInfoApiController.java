package com.atguigu.tingshu.album.api;

import com.atguigu.tingshu.album.service.TrackInfoService;
import com.atguigu.tingshu.common.result.Result;
import com.atguigu.tingshu.model.album.TrackInfo;
import com.atguigu.tingshu.query.album.TrackInfoQuery;
import com.atguigu.tingshu.vo.album.TrackInfoVo;
import com.atguigu.tingshu.vo.album.TrackListVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Tag(name = "声音管理")
@RestController
@AllArgsConstructor
@RequestMapping("/api/album/trackInfo")
@SuppressWarnings({"unchecked", "rawtypes"})
public class TrackInfoApiController {

    private TrackInfoService trackInfoService;

    /**
     * 创作音频 - 声音上传
     *
     * @return 结果
     */
    @PostMapping(value = "/uploadTrack")
    public Result uploadTrack(@RequestParam MultipartFile file) {
        return Result.ok(trackInfoService.uploadTrack(file));
    }

    /**
     * 创作音频 - 保存声音信息
     *
     * @param trackInfoVo 声音信息
     * @return
     */
    @PostMapping(value = "/saveTrackInfo")
    public Result saveTrackInfo(@RequestBody TrackInfoVo trackInfoVo) {
        trackInfoService.saveTrackInfo(trackInfoVo);
        return Result.ok();
    }

    /**
     * 分页查询当前用户声音列表
     *
     * @param trackInfoQuery
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/findUserTrackPage/{page}/{size}")
    public Result findUserTrackPage(@RequestBody TrackInfoQuery trackInfoQuery,
                                    @PathVariable(value = "page") Integer page,
                                    @PathVariable(value = "size") Integer size) {
        return Result.ok(trackInfoService.findUserTrackPage(trackInfoQuery, new Page<TrackListVo>(page, size)));
    }


    /**
     * 声音管理 - 删除声音
     *
     * @param trackId 声音id
     * @return
     */
    @DeleteMapping(value = "/removeTrackInfo/{trackId}")
    public Result removeTrackInfo(@PathVariable(value = "trackId") Long trackId) {
        trackInfoService.removeTrackInfo(trackId);
        return Result.ok();
    }

    /**
     * 声音管理 - 修改声音回显
     *
     * @param trackId 声音id
     * @return
     */
    @GetMapping(value = "/getTrackInfo/{trackId}")
    public Result getTrackInfo(@PathVariable(value = "trackId") Long trackId) {
        return Result.ok(trackInfoService.getOne(new LambdaQueryWrapper<TrackInfo>()
                .eq(TrackInfo::getUserId, 1L)
                .eq(TrackInfo::getId, trackId)));
    }

    /**
     * 声音管理 - 修改声音
     *
     * @param trackInfoVo 声音信息
     * @return 修改结果
     */
    @PutMapping(value = "/updateTrackInfo/{trackId}")
    public Result updateTrackInfo(@RequestBody TrackInfoVo trackInfoVo,
                                  @PathVariable(value = "trackId") Long trackId) {
        trackInfoService.updateTrackInfo(trackInfoVo, trackId);
        return Result.ok();
    }

    /**
     * 分页查询指定专辑的声音列表
     *
     * @param albumId 专辑id
     * @param page    当前页
     * @param size    每页显示记录数
     * @return 声音列表
     */
    @GetMapping(value = "/findAlbumTrackPage/{albumId}/{page}/{size}")
    public Result findAlbumTrackPage(@PathVariable(value = "albumId") Long albumId,
                                     @PathVariable(value = "page") Integer page,
                                     @PathVariable(value = "size") Integer size) {
        return Result.ok(trackInfoService.findAlbumTrackPage(albumId, page, size));
    }

    /**
     * 获取声音统计信息
     *
     * @param trackId 声音id
     * @return 声音统计信息
     */
    @GetMapping(value = "/getTrackStatVo/{trackId}")
    public Result getTrackStaVo(@PathVariable(value = "trackId") Long trackId) {
        return Result.ok(trackInfoService.getTrackStatVo(trackId));
    }

    /**
     * 获取用户指定购买和可购买未购买的声音列表
     *
     * @param trackId 声音id
     * @return 声音列表
     */
    @GetMapping(value = "/findUserTrackPaidList/{trackId}")
    public Result findUserTrackPaidList(@PathVariable(value = "trackId") Long trackId) {
        return Result.ok(trackInfoService.findUserTrackPaidList(trackId));
    }
}

