package com.atguigu.tingshu.album.service;

import com.tencentcloudapi.vod.v20180717.models.MediaInfo;

public interface VodService {

    /**
     * 获取文件的详细信息 （云点播获取）
     *
     * @param fileId
     */
    public MediaInfo getTrackDetailInfo(String fileId);



    /**
     * 删除云点播的文件
     * @param mediaFileId
     */
    void removeFile(String mediaFileId);
}
