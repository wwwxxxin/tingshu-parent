package com.atguigu.tingshu.album.api;

import com.atguigu.tingshu.album.service.AlbumInfoService;
import com.atguigu.tingshu.common.login.GuiguLogin;
import com.atguigu.tingshu.common.result.Result;
import com.atguigu.tingshu.query.album.AlbumInfoQuery;
import com.atguigu.tingshu.vo.album.AlbumInfoVo;
import com.atguigu.tingshu.vo.album.AlbumListVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Tag(name = "专辑管理")
@RestController
@RequestMapping("/api/album/albumInfo")
@AllArgsConstructor
@SuppressWarnings({"unchecked", "rawtypes"})
public class AlbumInfoApiController {

    private AlbumInfoService albumInfoService;

    /**
     * 保存专辑信息
     *
     * @param albumInfoVo 专辑信息
     * @return 保存结果
     */
    @GuiguLogin
    @PostMapping("/saveAlbumInfo")
    public Result saveAlbumInfo(@RequestBody AlbumInfoVo albumInfoVo) {
        albumInfoService.saveAlbumInfo(albumInfoVo);
        return Result.ok();
    }

    /**
     * 分页查询专辑列表
     *
     * @param page 分页参数
     * @param size 分页参数
     * @return 专辑列表
     */
    @GuiguLogin
    @PostMapping(value = "/findUserAlbumPage/{page}/{size}")
    public Result findUserAlbumPage(@PathVariable(value = "page") Integer page,
                                    @PathVariable(value = "size") Integer size,
                                    @RequestBody AlbumInfoQuery albumInfoQuery) {
        return Result.ok(albumInfoService.findAlbumInfoPage(new Page<AlbumListVo>(page, size), albumInfoQuery));
    }

    /**
     * 删除专辑
     *
     * @param albumId 专辑id
     * @return 删除结果
     */
    @GuiguLogin
    @DeleteMapping(value = "/removeAlbumInfo/{albumId}")
    public Result removeAlbumInfo(@PathVariable(value = "albumId") Long albumId) {
        albumInfoService.removeAlbumInfo(albumId);
        return Result.ok();
    }

    /**
     * 修改专辑 -专辑信息回显
     *
     * @param albumId 专辑id
     * @return 专辑信息
     */
    @GuiguLogin
    @GetMapping(value = "/getAlbumInfo/{albumId}")
    public Result getAlbumInfo(@PathVariable(value = "albumId") Long albumId) {
        return Result.ok(albumInfoService.getAlbumInfo(albumId));
    }

    /**
     * 修改专辑 - 提交更新
     *
     * @param albumId     专辑id
     * @param albumInfoVo 专辑信息
     * @return 修改结果
     */
    @GuiguLogin
    @PutMapping(value = "/updateAlbumInfo/{albumId}")
    public Result updateAlbumInfo(@PathVariable(value = "albumId") Long albumId, @RequestBody AlbumInfoVo albumInfoVo) {
        albumInfoService.updateAlbumInfo(albumId, albumInfoVo);
        return Result.ok();
    }


    /**
     * 创作音频-查询用户专辑列表
     *
     * @return 查询结果
     */
    @GuiguLogin
    @GetMapping(value = "/findUserAllAlbumList")
    public Result findUserAllAlbumList() {
        return Result.ok(albumInfoService.findUserAllAlbumList());
    }

}

