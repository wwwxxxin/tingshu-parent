package com.atguigu.tingshu.album.api;

import com.atguigu.tingshu.album.util.FileUtil;
import com.atguigu.tingshu.common.result.Result;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Tag(name = "上传管理接口")
@RestController
@RequestMapping("/api/album")
@AllArgsConstructor
public class FileUploadApiController {

    private FileUtil fileUtil;

    /**
     * 文件上传
     *
     * @param file 文件
     * @return 文件访问地址
     */
    @PostMapping("/fileUpload")
    public Result<String> fileUpload(@RequestParam MultipartFile file) {
        return Result.ok(fileUtil.fileUpload(file));
    }

}
