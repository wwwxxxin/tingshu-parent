package com.atguigu.tingshu.album.service;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.tingshu.model.album.BaseAttribute;
import com.atguigu.tingshu.model.album.BaseCategory3;
import com.atguigu.tingshu.model.album.BaseCategoryView;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/***
 * 分类管理 服务接口
 */

public interface BaseCategoryService extends IService<BaseCategoryView> {


    /**
     * 查询所有一级二级三级分类
     */
    public List<JSONObject> getCategoryInfo();


    /**
     * 查询标签数据
     *
     * @param category1Id 一级分类id
     * @return 标签数据
     */
    public List<BaseAttribute> findAttribute(Long category1Id);

    /**
     * 查询首页一级分类的热门Top7三级分类信息
     *
     * @param category1Id 一级分类ID
     * @return 三级分类信息
     */
    List<BaseCategory3> findTopBaseCategory3(Long category1Id);


    /**
     * 查询首页一级分类全部二级三级分类信息
     *
     * @param category1Id 一级分类ID
     * @return 二级三级分类信息
     */
    Object getBaseCategoryList(Long category1Id);
}
