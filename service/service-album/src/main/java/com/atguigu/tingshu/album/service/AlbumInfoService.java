package com.atguigu.tingshu.album.service;

import com.atguigu.tingshu.model.album.AlbumAttributeValue;
import com.atguigu.tingshu.model.album.AlbumInfo;
import com.atguigu.tingshu.query.album.AlbumInfoQuery;
import com.atguigu.tingshu.vo.album.AlbumInfoVo;
import com.atguigu.tingshu.vo.album.AlbumListVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

public interface AlbumInfoService extends IService<AlbumInfo> {


    /**
     * 保存专辑信息
     *
     * @param albumInfoVo
     */
    void saveAlbumInfo(AlbumInfoVo albumInfoVo);

    /**
     * 分页查询专辑的数据
     *
     * @param page           分页参数
     * @param albumInfoQuery 查询条件
     * @return 专辑数据
     */
    Page<AlbumListVo> findAlbumInfoPage(Page<AlbumListVo> page, AlbumInfoQuery albumInfoQuery);

    /**
     * 删除专辑
     *
     * @param albumId 专辑id
     */
    void removeAlbumInfo(Long albumId);

    /**
     * 修改-回显专辑信息
     *
     * @param albumId 专辑id
     * @return 专辑信息
     */
    Object getAlbumInfo(Long albumId);

    /**
     * 修改专辑 - 提交更新
     *
     * @param albumId     专辑id
     * @param albumInfoVo 专辑信息
     * @return 修改结果
     */
    void updateAlbumInfo(Long albumId, AlbumInfoVo albumInfoVo);

    /**
     * 创作音频-查询用户专辑列表
     *
     * @return
     */
    Object findUserAllAlbumList();

    /**
     * 查询专辑的标签列表
     *
     * @param albumId 专辑id
     * @return 专辑的标签列表
     */
    List<AlbumAttributeValue> getAlbumAttributeValueList(Long albumId);

    /**
     * 查询专辑的统计信息
     *
     * @param albumId 专辑id
     * @return 专辑的统计信息
     */
    Map<String, Object> getAlbumStatMap(Long albumId);
}
