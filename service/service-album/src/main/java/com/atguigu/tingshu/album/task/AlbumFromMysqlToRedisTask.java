package com.atguigu.tingshu.album.task;


import com.alibaba.fastjson.JSONObject;
import com.atguigu.tingshu.album.service.AlbumInfoService;
import com.atguigu.tingshu.album.service.BaseCategoryService;
import com.atguigu.tingshu.model.album.AlbumInfo;
import com.atguigu.tingshu.user.client.UserClientFeign;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.redisson.api.RBloomFilter;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/***
 * 定时任务：将数据库中的专辑数据同步到redis中
 */
@Log4j2
@Component
@AllArgsConstructor
public class AlbumFromMysqlToRedisTask {

    public final static Integer SIZE = 1000;

    private AlbumInfoService albumInfoService;

    private BaseCategoryService baseCategoryService;

    private UserClientFeign userClientFeign;

    private RedisTemplate redisTemplate;

    private RBloomFilter initBloomFilter;

    /**
     * 定时任务：专辑数据同步redis预热
     */
    @SneakyThrows
    @Scheduled(cron = "10/10 * * * * *")
    public void albumFromMysqlToRedis() {

        log.info("开始同步专辑数据到redis中");

        // 查询mysql中的专辑数据：is_delete=0
        long count = albumInfoService.count();
        // 分页查询专辑数据
        long size = count % SIZE == 0 ? count / SIZE : count / SIZE + 1;
        // 循环查询
        for (int i = 1; i <= size; i++) {
            // 根据当前页码和每页大小查询专辑数据
            List<AlbumInfo> records = albumInfoService.page(new Page<>(i, SIZE)).getRecords();
            // 循环写redis
            records.stream().forEach(albumInfo -> {
                // 专辑数据写redis 键getAlbumInfo:id 值:序列化专辑对象 有效期1天
                redisTemplate.opsForValue().set("getAlbumInfo:" + Arrays.asList(albumInfo.getId()).toString(),
                        JSONObject.toJSONString(albumInfo), 1, TimeUnit.DAYS);

                // 布隆过滤器添加专辑数据
                initBloomFilter.add("getAlbumInfo:" + Arrays.asList(albumInfo.getId()).toString());

                // 统计的数据写redis
                Map<String, Object> albumStatMap = albumInfoService.getAlbumStatMap(albumInfo.getId());
                redisTemplate.opsForValue().set("getAlbumStatMap:" + Arrays.asList(albumInfo.getId()).toString(),
                        JSONObject.toJSONString(albumStatMap), 1, TimeUnit.DAYS);

                // 作者数据写redis
                userClientFeign.getUserInfo(albumInfo.getUserId());
            });
        }
    }

    /**
     * 定时任务：分类数据同步redis预热
     */
    @Scheduled(cron = "10/10 * * * * *")
    public void categoryFromMysqlToRedis() {
        // 查询分类视图
        baseCategoryService.list().stream().forEach(baseCategoryView -> {
            // 循环将所有分类的数据写入redis中去
            redisTemplate.opsForValue().set(
                    "getBaseCategoryView:" + Arrays.asList(baseCategoryView.getCategory3Id()).toString(),
                    JSONObject.toJSONString(baseCategoryView));
        });
    }
}
