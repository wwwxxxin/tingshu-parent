package com.atguigu.tingshu.album.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.tingshu.album.mapper.BaseAttributeMapper;
import com.atguigu.tingshu.album.mapper.BaseCategory3Mapper;
import com.atguigu.tingshu.album.mapper.BaseCategoryViewMapper;
import com.atguigu.tingshu.album.service.BaseCategoryService;
import com.atguigu.tingshu.model.album.BaseAttribute;
import com.atguigu.tingshu.model.album.BaseCategory3;
import com.atguigu.tingshu.model.album.BaseCategoryView;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/***
 * 分类管理 服务接口实现
 */
@Service
@AllArgsConstructor
@SuppressWarnings({"unchecked", "rawtypes"})
public class BaseCategoryServiceImpl extends ServiceImpl<BaseCategoryViewMapper, BaseCategoryView> implements BaseCategoryService {

    private BaseAttributeMapper baseAttributeMapper;

    private BaseCategory3Mapper baseCategory3Mapper;

    /**
     * 查询所有一级二级三级分类
     */
    @Override
    public List<JSONObject> getCategoryInfo() {
        // 查询所有的分类数据：分类视图
        List<BaseCategoryView> baseCategoryViewsList1 = list(null);

        // 根据一级分类和二级分类归纳出一对多的关系
        // 1.一级分桶： ①使用stream流遍历集合 ②根据BaseCategoryView类型的一级id将集合分桶
        Map<Long, List<BaseCategoryView>> category1Map = baseCategoryViewsList1.stream()
                .collect(Collectors.groupingBy(BaseCategoryView::getCategory1Id));
        // 2.遍历一级桶：
        List<JSONObject> category1JsonList = category1Map.entrySet().stream().map(category1 -> {
            JSONObject category1Json = new JSONObject();

            // 获取元素的key
            Long category1Id = category1.getKey();
            category1Json.put("categoryId", category1Id);

            // 获取元素的value
            List<BaseCategoryView> baseCategoryViewsList2 = category1.getValue();

            // 获取一级分类名称
            String category1Name = baseCategoryViewsList2.get(0).getCategory1Name();
            category1Json.put("categoryName", category1Name);

            // 3.二级分桶：
            Map<Long, List<BaseCategoryView>> category2Map = baseCategoryViewsList2.stream()
                    .collect(Collectors.groupingBy(BaseCategoryView::getCategory2Id));
            // 4.遍历二级桶：
            List<JSONObject> category2JsonList = category2Map.entrySet().stream().map(category2 -> {

                JSONObject category2Json = new JSONObject();
                // 获取元素的key
                Long category2Id = category2.getKey();
                category2Json.put("categoryId", category2Id);

                // 获取元素的value
                List<BaseCategoryView> baseCategoryViewsList3 = category2.getValue();

                // 获取二级分类名称
                String category2Name = baseCategoryViewsList3.get(0).getCategory2Name();
                category2Json.put("categoryName", category2Name);


                // 5.获取三级分类数据
                List<JSONObject> category3JsonList = baseCategoryViewsList3.stream().map(category3 -> {
                    // Map category3Map = new HashMap();
                    JSONObject category3Json = new JSONObject();

                    // 获取三级分类名称
                    String category3Name = category3.getCategory3Name();
                    category3Json.put("categoryName", category3Name);

                    // 获取三级分类id
                    Long category3Id = category3.getCategory3Id();
                    category3Json.put("categoryId", category3Id);

                    // 返回给前端
                    return category3Json;
                }).collect(Collectors.toList());

                // 6. 保存
                category2Json.put("categoryChild", category3JsonList);
                // 返回
                return category2Json;
            }).collect(Collectors.toList());

            category1Json.put("categoryChild", category2JsonList);
            // 返回
            return category1Json;
        }).collect(Collectors.toList());
        return category1JsonList;
    }

    /**
     * 查询标签数据
     *
     * @param category1Id 一级分类id
     * @return 标签数据
     */
    @Override
    public List<BaseAttribute> findAttribute(Long category1Id) {
        return baseAttributeMapper.selectBaseAttributeBycategory1Id(category1Id);
    }

    /**
     * 查询首页一级分类的热门Top7三级分类信息
     *
     * @param category1Id 一级分类ID
     * @return 三级分类信息
     */
    @Override
    public List<BaseCategory3> findTopBaseCategory3(Long category1Id) {
        return baseCategory3Mapper.selectTopBaseCategory3(category1Id);
    }

    /**
     * 查询首页一级分类全部二级三级分类信息
     *
     * @param category1Id 一级分类ID
     * @return 二级三级分类信息
     */
    @Override
    public Object getBaseCategoryList(Long category1Id) {

        JSONObject json1Object = new JSONObject();

        // 根据一级分类id查询该一级分类下全部的二级和三级分类
        List<BaseCategoryView> baseCategoryView2List =
                list(new LambdaQueryWrapper<BaseCategoryView>()
                        .eq(BaseCategoryView::getCategory1Id, category1Id));
        json1Object.put("categoryId", category1Id);
        json1Object.put("categoryName", baseCategoryView2List.get(0).getCategory1Name());

        // 根据二级分类id分桶
        Map<Long, List<BaseCategoryView>> category2Map =
                baseCategoryView2List.stream().collect(Collectors.groupingBy(BaseCategoryView::getCategory2Id));
        // 遍历
        List<JSONObject> list2 = category2Map.entrySet().stream().map(category2 -> {
            JSONObject json2Object = new JSONObject();
            // 获取二级分类的id
            Long category2Id = category2.getKey();
            json2Object.put("categoryId", category2Id);
            // 获取二级对应的三级列表
            List<BaseCategoryView> baseCategoryView3List = category2.getValue();
            // 获取二级分类的名字
            String category2Name = baseCategoryView3List.get(0).getCategory2Name();
            json2Object.put("categoryName", category2Name);
            // 获取全部的三级分类的信息
            List<JSONObject> list = baseCategoryView3List.stream().map(baseCategoryView -> {
                JSONObject json3Object = new JSONObject();
                json3Object.put("categoryId", baseCategoryView.getCategory3Id());
                json3Object.put("categoryName", baseCategoryView.getCategory3Name());
                return json3Object;
            }).collect(Collectors.toList());
            // 保存子分类
            json2Object.put("categoryChild", list);
            // 返回
            return json2Object;
        }).collect(Collectors.toList());
        json1Object.put("categoryChild", list2);
        // 返回
        return json1Object;
    }

}
