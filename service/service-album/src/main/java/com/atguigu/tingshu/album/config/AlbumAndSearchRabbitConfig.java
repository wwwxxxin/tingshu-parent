package com.atguigu.tingshu.album.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/***
 * 专辑和搜索微服务数据同步的RabbitMQ配置
 */
@Configuration
public class AlbumAndSearchRabbitConfig {

    /**
     * 创建交换机
     */
    @Bean("albumAndSearchExchange")
    public Exchange albumAndSearchExchange() {
        return ExchangeBuilder.directExchange("album_and_search_exchange").build();
    }

    /**
     * 上架队列
     */
    @Bean("albumAndSearchQueueUpper")
    public Queue albumAndSearchQueueUpper() {
        return QueueBuilder.durable("album_and_search_queue_upper").build();
    }

    /**
     * 上架队列绑定
     */
    @Bean
    public Binding albumAndSearchQueueUpperBinding(@Qualifier("albumAndSearchQueueUpper") Queue albumAndSearchQueueUpper,
                                                   @Qualifier("albumAndSearchExchange") Exchange albumAndSearchExchange) {
        return BindingBuilder.bind(albumAndSearchQueueUpper)
                .to(albumAndSearchExchange)
                .with("album.upper")
                .noargs();
    }

    /**
     * 下架队列
     */
    @Bean("albumAndSearchQueueDown")
    public Queue albumAndSearchQueueDown() {
        return QueueBuilder.durable("album_and_search_queue_down").build();
    }

    /**
     * 下架队列绑定
     */
    @Bean
    public Binding albumAndSearchQueueDownBinding(@Qualifier("albumAndSearchQueueDown") Queue albumAndSearchQueueDown,
                                                  @Qualifier("albumAndSearchExchange") Exchange albumAndSearchExchange) {
        return BindingBuilder.bind(albumAndSearchQueueDown)
                .to(albumAndSearchExchange)
                .with("album.down")
                .noargs();
    }

    /**
     * redis队列
     */
    @Bean("albumAndSearchQueueRedis")
    public Queue albumAndSearchQueueRedis() {
        return QueueBuilder.durable("album_and_search_queue_redis").build();
    }

    /**
     * redis队列绑定
     */
    @Bean
    public Binding albumAndSearchQueueRedisBinding(@Qualifier("albumAndSearchQueueRedis") Queue albumAndSearchQueueRedis,
                                                   @Qualifier("albumAndSearchExchange") Exchange albumAndSearchExchange) {
        return BindingBuilder.bind(albumAndSearchQueueRedis)
                .to(albumAndSearchExchange)
                .with("album.redis")
                .noargs();
    }
}

