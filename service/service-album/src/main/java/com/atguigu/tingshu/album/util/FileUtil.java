package com.atguigu.tingshu.album.util;

import com.atguigu.tingshu.album.config.MinioConstantProperties;
import io.minio.*;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartFile;

/***
 * 文件工具类
 */
@Configuration
@AllArgsConstructor
public class FileUtil {

    private MinioConstantProperties minioConstantProperties;
    private MinioClient minioClient;



    /**
     * 文件上传
     *
     * @param file 文件
     * @return 文件访问地址
     */
    @SneakyThrows
    public String fileUpload(MultipartFile file) {

        // 判断存储该文件的桶是否存在
        if (!minioClient.bucketExists(BucketExistsArgs.builder()
                .bucket(minioConstantProperties.getBucketName())
                .build())
        ) {
            // 不存在则创建一个桶
            minioClient.makeBucket(MakeBucketArgs.builder()
                    .bucket(minioConstantProperties.getBucketName())
                    .build()
            );
        }

        // 1.计算文件的md5值
        String md5 = DigestUtils.md5Hex(file.getInputStream());
        // 2.根据md5值生成新的文件名：md5值+文件后缀名
        String fileName = md5 + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        // 3.检查minio是否存在相同文件名的文件
        try {
            // 如果有该文件就不会抛出异常
            minioClient.statObject(StatObjectArgs.builder()
                    .bucket(minioConstantProperties.getBucketName())
                    .object(fileName)
                    .build());
        } catch (Exception e) {
            // 抛出异常表示没有该文件，开始文件上传
            minioClient.putObject(PutObjectArgs.builder()
                    // 文件名称
                    .object(fileName)
                    // 文件数据
                    .stream(file.getInputStream(), file.getSize(), -1)
                    // 文件类型
                    .contentType(file.getContentType())
                    // 存储桶名称
                    .bucket(minioConstantProperties.getBucketName())
                    .build()
            );
        }
        // 返回文件的访问地址
        return minioConstantProperties.getEndpointUrl() + "/"
                + minioConstantProperties.getBucketName() + "/"
                + fileName;
    }

    /**
     * 删除文件
     *
     * @param fileName 文件名
     */
    @SneakyThrows
    public void deleteFile(String fileName) {
        minioClient.removeObject(RemoveObjectArgs.builder()
                .bucket(minioConstantProperties.getBucketName())
                .object(fileName)
                .build()
        );
    }

}


