package com.atguigu.tingshu.album.mapper;

import com.atguigu.tingshu.model.album.BaseCategory3;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BaseCategory3Mapper extends BaseMapper<BaseCategory3> {

    /**
     * 查询首页一级分类的热门Top7三级分类信息
     *
     * @param category1Id 一级分类ID
     * @return 三级分类信息
     */
    @Select("SELECT\n" +
            "\tt2.id,\n" +
            "\tt2.`name`,\n" +
            "\tt2.category2_id,\n" +
            "\tt2.order_num,\n" +
            "\tt2.is_top \n" +
            "FROM\n" +
            "\tbase_category2 t1\n" +
            "\tINNER JOIN base_category3 t2 ON t1.id = t2.category2_id \n" +
            "WHERE\n" +
            "\tcategory1_id = #{category1Id}  \n" +
            "\tAND t1.is_deleted = 0 \n" +
            "\tAND t2.is_deleted = 0 \n" +
            "ORDER BY\n" +
            "\tt2.is_top \n" +
            "\tLIMIT 0,7")
    public List<BaseCategory3> selectTopBaseCategory3(@Param("category1Id") Long category1Id);

}
