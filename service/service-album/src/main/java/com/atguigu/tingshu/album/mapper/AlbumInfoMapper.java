package com.atguigu.tingshu.album.mapper;

import com.atguigu.tingshu.model.album.AlbumInfo;
import com.atguigu.tingshu.query.album.AlbumInfoQuery;
import com.atguigu.tingshu.vo.album.AlbumListVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface AlbumInfoMapper extends BaseMapper<AlbumInfo> {

    /**
     * 分页查询专辑数据
     *
     * @param albumInfoQuery 查询条件
     * @param page           分页参数
     * @return 专辑数据
     */
    public Page<AlbumListVo> selectPageAlbumInfo(@Param("vo") AlbumInfoQuery albumInfoQuery, Page<AlbumListVo> page);

}

