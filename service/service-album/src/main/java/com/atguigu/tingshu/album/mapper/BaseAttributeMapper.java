package com.atguigu.tingshu.album.mapper;

import com.atguigu.tingshu.model.album.BaseAttribute;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BaseAttributeMapper extends BaseMapper<BaseAttribute> {


    /**
     * 查询标签数据
     *
     * @param category1Id 一级分类id
     * @return 标签数据
     */
    public List<BaseAttribute> selectBaseAttributeBycategory1Id(@Param("category1Id") long category1Id);
}
