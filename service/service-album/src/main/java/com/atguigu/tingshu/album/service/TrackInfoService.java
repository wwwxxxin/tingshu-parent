package com.atguigu.tingshu.album.service;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.tingshu.model.album.TrackInfo;
import com.atguigu.tingshu.query.album.TrackInfoQuery;
import com.atguigu.tingshu.vo.album.TrackInfoVo;
import com.atguigu.tingshu.vo.album.TrackListVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

public interface TrackInfoService extends IService<TrackInfo> {

    /**
     * 创作音频 -声音上传
     *
     * @return
     */
    Object uploadTrack(MultipartFile file);

    /**
     * 创作音频 - 保存声音信息
     *
     * @param trackInfoVo 声音信息
     */
    void saveTrackInfo(TrackInfoVo trackInfoVo);

    /**
     * 分页查询当前用户声音列表
     *
     * @param trackInfoQuery
     * @param page
     * @return
     */
    Object findUserTrackPage(TrackInfoQuery trackInfoQuery, Page<TrackListVo> page);

    /**
     * 声音管理 - 删除声音
     *
     * @param trackId
     */
    void removeTrackInfo(Long trackId);

    /**
     * 修改声音
     *
     * @param trackInfoVo
     * @param trackId
     */
    void updateTrackInfo(TrackInfoVo trackInfoVo, Long trackId);

    /**
     * 分页查询指定专辑的声音列表
     *
     * @param albumId 专辑id
     * @param page    当前页
     * @param size    每页显示记录数
     * @return 声音列表
     */
    Object findAlbumTrackPage(Long albumId, Integer page, Integer size);

    /**
     * 获取声音统计信息
     *
     * @param trackId 声音id
     * @return 声音统计信息
     */
    Object getTrackStatVo(Long trackId);

    /**
     * 获取用户指定购买和可购买未购买的声音列表
     *
     * @param trackId 声音id
     * @return 声音列表
     */
    Object findUserTrackPaidList(Long trackId);

    /**
     * 查询即将订单确认的声音信息
     *
     * @param trackId 声音id
     * @return 声音信息
     */
    JSONObject getTrackListAndPrice(Long trackId);
}


