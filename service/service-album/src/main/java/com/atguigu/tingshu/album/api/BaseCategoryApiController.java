package com.atguigu.tingshu.album.api;

import com.atguigu.tingshu.album.service.BaseCategoryService;
import com.atguigu.tingshu.common.result.Result;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * 分类管理 相关接口
 */
@Tag(name = "分类管理")
@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/album/category")
@SuppressWarnings({"unchecked", "rawtypes"})
public class BaseCategoryApiController {

    private BaseCategoryService baseCategoryService;

    /**
     * 查询分类信息
     *
     * @return 分类信息
     */
    @GetMapping("/getBaseCategoryList")
    public Result getBaseCategoryList() {
        return Result.ok(baseCategoryService.getCategoryInfo());
    }

    /**
     * 查询标签数据
     *
     * @param category1Id 一级分类id
     * @return 标签数据
     */
    @GetMapping("/findAttribute/{category1Id}")
    public Result findAttribute(@PathVariable(value = "category1Id") Long category1Id) {
        return Result.ok(baseCategoryService.findAttribute(category1Id));
    }

    /**
     * 查询首页一级分类的热门Top7三级分类信息
     *
     * @param category1Id 一级分类ID
     * @return 三级分类信息
     */
    @GetMapping("/findTopBaseCategory3/{category1Id}")
    public Result findTopBaseCategory3(@PathVariable(value = "category1Id") Long category1Id) {
        return Result.ok(baseCategoryService.findTopBaseCategory3(category1Id));
    }

    /**
     * 查询首页一级分类全部二级三级分类信息
     *
     * @param category1Id 一级分类ID
     * @return 二级三级分类信息
     */
    @GetMapping("/getBaseCategoryList/{category1Id}")
    public Result getBaseCategoryList(@PathVariable(value = "category1Id") Long category1Id) {
        return Result.ok(baseCategoryService.getBaseCategoryList(category1Id));
    }

}

