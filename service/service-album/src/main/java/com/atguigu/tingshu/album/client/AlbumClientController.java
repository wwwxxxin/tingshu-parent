package com.atguigu.tingshu.album.client;

import com.atguigu.tingshu.album.service.AlbumInfoService;
import com.atguigu.tingshu.album.service.BaseCategoryService;
import com.atguigu.tingshu.common.cache.GuiguCache;
import com.atguigu.tingshu.model.album.AlbumAttributeValue;
import com.atguigu.tingshu.model.album.AlbumInfo;
import com.atguigu.tingshu.model.album.BaseCategory3;
import com.atguigu.tingshu.model.album.BaseCategoryView;
import lombok.AllArgsConstructor;
import org.redisson.api.RBloomFilter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 专辑微服务提供的内部调用接口
 */
@RestController
@RequestMapping("/client/album/albumInfo")
@AllArgsConstructor
public class AlbumClientController {

    private AlbumInfoService albumInfoService;
    private BaseCategoryService baseCategoryService;
    private RBloomFilter initBloomFilter;

    /**
     * 查询专辑的数据
     *
     * @param albumId 专辑id
     * @return 专辑的数据
     */
    @GuiguCache(prefix = "albumInfo:")
    @GetMapping(value = "/getAlbumInfo/{albumId}")
    public AlbumInfo getAlbumInfo(@PathVariable(value = "albumId") Long albumId) {
        return albumInfoService.getById(albumId);
    }

    /**
     * 查询分类视图获取一级二级三级分类
     *
     * @param category3Id 三级分类id
     * @return 分类视图
     */
    @GuiguCache(prefix = "baseCategoryView:")
    @GetMapping(value = "/getBaseCategoryView/{category3Id}")
    public BaseCategoryView getBaseCategoryView(@PathVariable(value = "category3Id") Long category3Id) {
        return baseCategoryService.getById(category3Id);
    }

    /**
     * 查询专辑的标签列表
     *
     * @param albumId 专辑id
     * @return 专辑的标签列表
     */
    @GuiguCache(prefix = "albumAttributeValue:")
    @GetMapping(value = "/getAlbumAttributeValueList/{albumId}")
    public List<AlbumAttributeValue> getAlbumAttributeValueList(@PathVariable(value = "albumId") Long albumId) {
        return albumInfoService.getAlbumAttributeValueList(albumId);
    }

    /**
     * 查询专辑的统计信息
     *
     * @param albumId 专辑id
     * @return 专辑的统计信息
     */
    @GuiguCache(prefix = "albumStatMap:")
    @GetMapping(value = "/getAlbumStatMap/{albumId}")
    public Map<String, Object> getAlbumStatMap(@PathVariable(value = "albumId") Long albumId) {
        return albumInfoService.getAlbumStatMap(albumId);
    }

    /**
     * 查询首页一级分类的热门Top7三级分类信息
     *
     * @param category1Id 一级分类ID
     * @return 三级分类信息
     */
    @GuiguCache(prefix = "topBaseCategory3:")
    @GetMapping("/findTopBaseCategory3/{category1Id}")
    public List<BaseCategory3> findTopBaseCategory3(@PathVariable(value = "category1Id") Long category1Id) {
        return baseCategoryService.findTopBaseCategory3(category1Id);
    }

    /**
     * 获取布隆过滤器中是否包含专辑信息
     *
     * @param albumId 专辑id
     * @return 是否存在
     */
    @GetMapping("/getAlbumFromBloom/{albumId}")
    public Boolean getAlbumFromBloom(@PathVariable(value = "albumId") Long albumId) {
        return initBloomFilter.contains("getAlbumInfo:" + Arrays.asList(albumId));
    }
}
