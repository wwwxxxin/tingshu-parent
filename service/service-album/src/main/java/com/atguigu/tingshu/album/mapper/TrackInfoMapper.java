package com.atguigu.tingshu.album.mapper;

import com.atguigu.tingshu.model.album.TrackInfo;
import com.atguigu.tingshu.query.album.TrackInfoQuery;
import com.atguigu.tingshu.vo.album.AlbumTrackListVo;
import com.atguigu.tingshu.vo.album.TrackListVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface TrackInfoMapper extends BaseMapper<TrackInfo> {

    /**
     * 获取指定专辑的最大排序号
     *
     * @param albumId 专辑id
     * @return
     */
    @Select("SELECT order_num FROM track_info WHERE album_id = #{albumId} ORDER BY order_num DESC limit 1")
    public Integer selectTrackMaxOrderNum(@Param("albumId") Long albumId);

    /**
     * 分页查询当前用户声音列表
     *
     * @param trackInfoQuery
     * @param page
     * @return
     */
    public Page<TrackListVo> selectTrackInfo(@Param("vo") TrackInfoQuery trackInfoQuery, Page<TrackListVo> page);

    /**
     * 分页条件查询声音列表
     *
     * @param albumId 专辑id
     * @param page    当前页
     * @return 声音列表
     */
    public Page<AlbumTrackListVo> selectPageTrackInfoListByAlbumId(@Param("albumId") Long albumId, Page<AlbumTrackListVo> page);

}
