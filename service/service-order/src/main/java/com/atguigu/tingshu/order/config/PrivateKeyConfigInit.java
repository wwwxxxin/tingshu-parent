package com.atguigu.tingshu.order.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;

/***
 *  自定义配置类：用于读取私钥
 */
@Configuration
public class PrivateKeyConfigInit {

    /**
     * 私钥初始化
     *
     * @return 私钥
     */
    @Bean
    public RSAPrivateKey privateKeyConfig() {
        // 资源初始化
        ClassPathResource classPathResource = new ClassPathResource("tingshu.jks");
        // 读取私钥
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(classPathResource, "tingshu".toCharArray());
        // 读取文件中的两把钥匙
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair("tingshu", "tingshu".toCharArray());
        // 获取私钥 放入容器
        return (RSAPrivateKey) keyPair.getPrivate();
    }
}