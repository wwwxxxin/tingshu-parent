package com.atguigu.tingshu.order.service;

import com.atguigu.tingshu.model.order.OrderInfo;
import com.atguigu.tingshu.vo.order.OrderInfoVo;
import com.atguigu.tingshu.vo.order.TradeVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

public interface OrderInfoService extends IService<OrderInfo> {

    /**
     * 确认订单
     *
     * @param tradeVo 订单信息
     * @return 订单号
     */
    Object trade(TradeVo tradeVo);

    /**
     * 查询用户准备购买的声音列表
     *
     * @param userId 用户id
     * @return 声音列表
     */
    Map<String, Object> getUserTrackOrderInfo(Long userId);

    /**
     * 提交订单
     *
     * @param orderInfoVo 订单信息
     * @return 订单号
     */
    Object submitOrder(OrderInfoVo orderInfoVo);

    /**
     * 取消订单：超时取消/主动取消
     *
     * @param orderNo 订单号
     */
    void cancelOrder(String orderNo);

    /**
     * 分页条件查询指定用户的订单列表
     *
     * @param page 当前页
     * @param size 每页显示的记录数
     * @return 订单列表
     */
    Object findUserPage(Integer page, Integer size);

    /**
     * 查询订单的详细信息
     *
     * @param orderNo 订单号
     * @return 订单详细信息
     */
    Object getOrderInfo(String orderNo);

    /**
     * 获取订单支付信息（根据用户选择的支付方式）
     *
     * @param orderNo 订单号
     * @param payWay  支付方式
     * @return 订单支付信息
     */
    Object getOrderPayInfo(String orderNo, String payWay);

}
