package com.atguigu.tingshu.order.mapper;

import com.atguigu.tingshu.model.order.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {

    /**
     * 根据订单ID查询订单明细
     *
     * @param id 订单ID
     * @return 订单明细
     */
    @Select("select min(item_id) from order_detail where order_id = #{id} group by order_id")
    Long selectItemIdByOrderId(@Param("id") Long id);
}
