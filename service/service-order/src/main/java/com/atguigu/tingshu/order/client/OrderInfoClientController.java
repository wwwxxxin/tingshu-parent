package com.atguigu.tingshu.order.client;

import com.atguigu.tingshu.order.service.OrderInfoService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/***
 * 订单微服务供内部调用的控制层
 */
@RestController
@AllArgsConstructor
@RequestMapping(value = "/client/order/orderInfo")
public class OrderInfoClientController {

    private OrderInfoService orderInfoService;

    /**
     * 查询用户准备购买的声音列表
     *
     * @param userId 用户id
     * @return 声音列表
     */
    @GetMapping(value = "/getUserTrackOrderInfo/{userId}")
    public Map<String, Object> getUserTrackOrderInfo(@PathVariable(value = "userId") Long userId) {
        return orderInfoService.getUserTrackOrderInfo(userId);
    }
}