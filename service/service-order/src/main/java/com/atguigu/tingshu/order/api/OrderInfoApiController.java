package com.atguigu.tingshu.order.api;

import com.atguigu.tingshu.common.result.Result;
import com.atguigu.tingshu.order.service.OrderInfoService;
import com.atguigu.tingshu.vo.order.OrderInfoVo;
import com.atguigu.tingshu.vo.order.TradeVo;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Tag(name = "订单管理")
@RestController
@RequestMapping("/api/order/orderInfo")
@AllArgsConstructor
@SuppressWarnings({"unchecked", "rawtypes"})
public class OrderInfoApiController {

    private OrderInfoService orderInfoService;

    /**
     * 确认订单
     *
     * @param tradeVo 订单信息
     * @return 订单号
     */
    @PostMapping(value = "/trade")
    public Result trade(@RequestBody TradeVo tradeVo) {
        return Result.ok(orderInfoService.trade(tradeVo));
    }

    /**
     * 提交订单
     *
     * @param orderInfoVo 订单信息
     * @return 订单号
     */
    @PostMapping(value = "/submitOrder")
    public Result submitOrder(@RequestBody OrderInfoVo orderInfoVo) {
        return Result.ok(orderInfoService.submitOrder(orderInfoVo));
    }

    /**
     * 主动取消订单
     *
     * @param orderNo 订单号
     * @return 结果
     */
    @GetMapping(value = "/cancelOrder/{orderNo}")
    public Result cancelOrder(@PathVariable(value = "orderNo") String orderNo) {
        orderInfoService.cancelOrder(orderNo);
        return Result.ok();
    }

    /**
     * 分页条件查询指定用户的订单列表
     *
     * @param page 当前页
     * @param size 每页显示的记录数
     * @return 订单列表
     */
    @GetMapping(value = "/findUserPage/{page}/{size}")
    public Result findUserPage(@PathVariable(value = "page") Integer page,
                               @PathVariable(value = "size") Integer size) {
        return Result.ok(orderInfoService.findUserPage(page, size));
    }

    /**
     * 查询订单的详细信息
     *
     * @param orderNo 订单号
     * @return 订单详细信息
     */
    @GetMapping(value = "/getOrderInfo/{orderNo}")
    public Result getOrderInfo(@PathVariable(value = "orderNo") String orderNo) {
        return Result.ok(orderInfoService.getOrderInfo(orderNo));
    }

    /**
     * 获取订单支付信息（根据用户选择的支付方式）
     *
     * @param orderNo 订单号
     * @param payWay  支付方式
     * @return 订单支付信息
     */
    @GetMapping(value = "/getOrderPayInfo/{orderNo}/{payWay}")
    public Result getOrderPayInfo(@PathVariable(value = "orderNo") String orderNo,
                                  @PathVariable(value = "payWay") String payWay) {
        return Result.ok(orderInfoService.getOrderPayInfo(orderNo, payWay));
    }
}

