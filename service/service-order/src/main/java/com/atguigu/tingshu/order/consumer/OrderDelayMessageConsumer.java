package com.atguigu.tingshu.order.consumer;

import com.atguigu.tingshu.order.service.OrderInfoService;
import com.rabbitmq.client.Channel;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/***
 * 订单延时消息 消费者
 */
@Log4j2
@Component
@AllArgsConstructor
public class OrderDelayMessageConsumer {

    private OrderInfoService orderInfoService;

    private RedisTemplate redisTemplate;

    /**
     * 接受延迟消息: 取消未支付的订单
     *
     * @param channel 通道
     * @param message 消息
     */
    @SneakyThrows
    @RabbitListener(queues = "order_delay_queue")
    public void timeOutCancelOrder(Channel channel, Message message) {
        // 获取消息内容
        String orderNo = new String(message.getBody());
        // 获取消息属性
        MessageProperties messageProperties = message.getMessageProperties();
        // 获取消息编号
        long deliveryTag = messageProperties.getDeliveryTag();
        try {
            // 超时取消订单
            orderInfoService.cancelOrder(orderNo);
            // 确认消息
            channel.basicAck(deliveryTag, false);
        } catch (Exception e) {
            // 判断是否第一次,如果不是记录mysql/redis/日志
            if (messageProperties.getRedelivered()) {
                redisTemplate.opsForSet().add("TimeOutCancelOrderFail", orderNo);
                // 丢弃消息
                channel.basicReject(deliveryTag, false);
            } else {
                // 否则再来一次
                channel.basicReject(deliveryTag, true);
            }
        }
    }
}