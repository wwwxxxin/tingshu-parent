package com.atguigu.tingshu.order.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/***
 *  订单延时消息配置类
 */
@Configuration
public class OrderDelayMessageConfig {

    /**
     * 普通交换机
     */
    @Bean("orderDelayExchange")
    public Exchange orderDelayExchange() {
        return ExchangeBuilder.directExchange("order_delay_exchange").build();
    }

    /**
     * 死信队列
     */
    @Bean("orderDelayDeadQueue")
    public Queue orderDelayDeadQueue() {
        return QueueBuilder
                .durable("order_delay_dead_queue")
                .withArgument("x-dead-letter-exchange", "order_delay_dead_exchange")
                .withArgument("x-dead-letter-routing-key", "order.delay.normal")
                .build();
    }

    /**
     * 普通交换机和死信队列绑定
     */
    @Bean
    public Binding orderDelayDeadQueueBinding(@Qualifier("orderDelayExchange") Exchange orderDelayExchange,
                                              @Qualifier("orderDelayDeadQueue") Queue orderDelayDeadQueue) {
        return BindingBuilder.bind(orderDelayDeadQueue).to(orderDelayExchange).with("order.delay.dead").noargs();
    }

    /**
     * 死信交换机
     */
    @Bean("orderDelayDeadExchange")
    public Exchange orderDelayDeadExchange() {
        return ExchangeBuilder.directExchange("order_delay_dead_exchange").build();
    }

    /**
     * 普通队列
     */
    @Bean("orderDelayQueue")
    public Queue orderDelayQueue() {
        return QueueBuilder.durable("order_delay_queue").build();
    }

    /**
     * 死信交换机和普通队列绑定
     */
    @Bean
    public Binding orderDelayQueueBinding(@Qualifier("orderDelayDeadExchange") Exchange orderDelayDeadExchange,
                                          @Qualifier("orderDelayQueue") Queue orderDelayQueue) {
        return BindingBuilder.bind(orderDelayQueue).to(orderDelayDeadExchange).with("order.delay.normal").noargs();
    }
}
