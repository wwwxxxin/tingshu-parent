package com.atguigu.tingshu.order.mapper;

import com.atguigu.tingshu.model.order.OrderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

    /**
     * 查询用户本次购买的专辑未支付/已支付的订单
     *
     * @param userId 用户id
     * @return 订单数量
     */
    @Select("SELECT\n" +
            "\tcount( 1 ) \n" +
            "FROM\n" +
            "\torder_info t1\n" +
            "\tINNER JOIN order_detail t2 ON t1.id = t2.order_id \n" +
            "WHERE\n" +
            "\tt1.user_id = #{userId} \n" +
            "\tAND t1.item_type = '1001' \n" +
            "\tAND t2.item_id = #{albumId} \n" +
            "\tAND ( t1.order_status = '0901' OR t1.order_status = '0902' ) \n" +
            "\tAND t1.is_deleted = 0 \n" +
            "\tAND t2.is_deleted = 0")
    int selectAlbumUnpaidOrderCount(@Param("userId") Long userId,
                                    @Param("albumId") Long albumId);

    /**
     * 查询指定用户未支付的声音列表
     *
     * @param userId 用户id
     * @return 声音id列表
     */
    @Select("SELECT\n" +
            "\tt2.item_id \n" +
            "FROM\n" +
            "\torder_info t1\n" +
            "\tINNER JOIN order_detail t2 ON t1.id = t2.order_id \n" +
            "WHERE\n" +
            "\tt1.item_type = '1002' \n" +
            "\tAND t1.order_status = '0901'\n" +
            "\tand\n" +
            "\tt1.user_id = #{userId}\n" +
            "\tand \n" +
            "\tt1.is_deleted = 0\n" +
            "\tand\n" +
            "\tt2.is_deleted = 0")
    List<Long> selectTrackIdList(@Param("userId") Long userId);

    /**
     * 分页查询指定用户的订单列表
     *
     * @param page  分页信息
     * @param userId 用户id
     * @return 订单列表
     */
    Page<OrderInfo> selectUserOrderPage(Page<OrderInfo> page, @Param("userId") Long userId);
}
