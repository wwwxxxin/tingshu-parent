package com.atguigu.tingshu.user.service;

import com.atguigu.tingshu.model.user.UserPaidTrack;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 声音购买记录 接口
 */
public interface UserPaidTrackService extends IService<UserPaidTrack> {

    /**
     * 查询用户购买过的指定专辑的声音的键值对
     *
     * @param albumId 专辑id
     * @param userId  用户id
     * @return 声音的键值对
     */
    Map<String, Object> getUserPaidTrack(Long albumId, Long userId);
}
