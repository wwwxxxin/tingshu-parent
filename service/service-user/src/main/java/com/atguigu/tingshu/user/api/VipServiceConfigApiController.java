package com.atguigu.tingshu.user.api;

import com.atguigu.tingshu.common.result.Result;
import com.atguigu.tingshu.user.service.VipServiceConfigService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "VIP服务配置管理接口")
@RestController
@AllArgsConstructor
@RequestMapping("/api/user/vipServiceConfig")
@SuppressWarnings({"unchecked", "rawtypes"})
public class VipServiceConfigApiController {

    private VipServiceConfigService vipServiceConfigService;


    /**
     * 查询vip配置
     *
     * @return vip配置
     */
    @GetMapping(value = "/findAll")
    public Result findAll() {
        return Result.ok(vipServiceConfigService.list());
    }
}

