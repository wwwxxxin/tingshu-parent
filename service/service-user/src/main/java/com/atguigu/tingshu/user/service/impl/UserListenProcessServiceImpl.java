package com.atguigu.tingshu.user.service.impl;

import com.atguigu.tingshu.common.util.AuthContextHolder;
import com.atguigu.tingshu.model.user.UserListenProcess;
import com.atguigu.tingshu.model.user.UserListenProcessTable;
import com.atguigu.tingshu.user.mapper.UserListenProcessMapper;
import com.atguigu.tingshu.user.service.UserListenProcessService;
import com.atguigu.tingshu.vo.user.UserListenProcessVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Service
@AllArgsConstructor
@SuppressWarnings({"unchecked", "rawtypes"})
public class UserListenProcessServiceImpl implements UserListenProcessService {

    private MongoTemplate mongoTemplate;
    private final UserListenProcessMapper userListenProcessMapper;

    /**
     * 查询用户声音播放进度
     *
     * @param trackId 声音id
     * @return 进度
     */
    @Override
    public Object getTrackBreakSecond(Long trackId) {
        // 查询用户的上一次播放的进度
        UserListenProcessTable userListenProcessTable = userListenProcessMapper.selectOne(
                new LambdaQueryWrapper<UserListenProcessTable>()
                        .eq(UserListenProcessTable::getUserId, AuthContextHolder.getUserId())
                        .eq(UserListenProcessTable::getTrackId, trackId));
        if (userListenProcessTable != null) {
            // 上次听过
            return userListenProcessTable.getBreakSecond();
        }
        // 没听过
        return new BigDecimal(0);
    }


    /**
     * 更新用户声音播放进度 -- 从数据库中查询
     *
     * @param userListenProcessVo 进度

     @Override public void updateListenProcess(UserListenProcessVo userListenProcessVo) {
     // 判断是否为空
     if (userListenProcessVo != null) {
     return;
     }
     // 删除原来的记录
     userListenProcessMapper.delete(new LambdaQueryWrapper<UserListenProcessTable>()
     .eq(UserListenProcessTable::getUserId, AuthContextHolder.getUserId())
     .eq(UserListenProcessTable::getAlbumId, userListenProcessVo.getAlbumId())
     .eq(UserListenProcessTable::getTrackId, userListenProcessVo.getTrackId())
     );
     //新增记录
     UserListenProcessTable userListenProcessTable = new UserListenProcessTable();
     userListenProcessTable.setUserId(AuthContextHolder.getUserId());
     userListenProcessTable.setAlbumId(userListenProcessVo.getAlbumId());
     userListenProcessTable.setTrackId(userListenProcessVo.getTrackId());
     userListenProcessTable.setBreakSecond(userListenProcessVo.getBreakSecond());
     userListenProcessMapper.insert(userListenProcessTable);
     }
     */

    /**
     * 更新用户声音播放进度
     *
     * @param userListenProcessVo 进度
     */
    @Override
    public void updateListenProcess(UserListenProcessVo userListenProcessVo) {

        // 判断是否为空
        if (userListenProcessVo != null) {
            // 构建集合名称：对象名 + 声音id
            String collectName = "userListenProcess_" + userListenProcessVo.getTrackId();
            // 查询mongodb 用户是否听过这声音
            UserListenProcess userListenProcess = mongoTemplate.findOne(
                    // 条件构造器
                    new Query().addCriteria(
                            Criteria.where("userId").is(AuthContextHolder.getUserId())
                                    .and("trackId").is(userListenProcessVo.getTrackId())
                                    .and("albumId").is(userListenProcessVo.getAlbumId())
                    ), UserListenProcess.class, collectName);

            if (userListenProcess == null) {
                // 没听过：创建一个新的收听记录
                userListenProcess = new UserListenProcess();
                userListenProcess.setId(UUID.randomUUID().toString().replace("-", ""));
                userListenProcess.setUserId(AuthContextHolder.getUserId());
                userListenProcess.setTrackId(userListenProcessVo.getTrackId());
                userListenProcess.setAlbumId(userListenProcessVo.getAlbumId());
                userListenProcess.setBreakSecond(userListenProcessVo.getBreakSecond());
                userListenProcess.setCreateTime(new Date());
            }
            userListenProcess.setBreakSecond(userListenProcessVo.getBreakSecond());
            userListenProcess.setUpdateTime(new Date());
            // userListenProcess.setIsShow(0);
            // 直接覆盖
            mongoTemplate.save(userListenProcess, collectName);
            // TODO-增加播放次数-同声音一人一天一次

        }
    }
}
