package com.atguigu.tingshu.user.client;


import com.atguigu.tingshu.common.cache.GuiguCache;
import com.atguigu.tingshu.model.user.UserInfo;
import com.atguigu.tingshu.model.user.UserPaidAlbum;
import com.atguigu.tingshu.model.user.VipServiceConfig;
import com.atguigu.tingshu.user.service.UserInfoService;
import com.atguigu.tingshu.user.service.UserPaidAlbumService;
import com.atguigu.tingshu.user.service.UserPaidTrackService;
import com.atguigu.tingshu.user.service.VipServiceConfigService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/***
 * 用户微服务提供的内部接口
 */
@RestController
@RequestMapping(value = "/client/user/userInfo")
@AllArgsConstructor
public class UserClientController {

    private UserInfoService userInfoService;

    private UserPaidAlbumService userPaidAlbumService;

    private UserPaidTrackService userPaidTrackService;

    private VipServiceConfigService vipServiceConfigService;

    /**
     * 查询用户的详细信息
     *
     * @param userId 用户id
     * @return 用户的详细信息
     */
    @GuiguCache(prefix = "userInfo:")
    @GetMapping(value = "/getUserInfo/{userId}")
    public UserInfo getUserInfo(@PathVariable(value = "userId") Long userId) {
        return userInfoService.getById(userId);
    }

    /**
     * 查询用户的专辑购买记录
     *
     * @param albumId 专辑id
     * @param userId  用户id
     * @return 是否购买
     */
    @GetMapping(value = "/getUserPaidAlbum/{albumId}/{userId}")
    public Boolean getUserPaidAlbum(@PathVariable(value = "albumId") Long albumId,
                                    @PathVariable(value = "userId") Long userId) {
        return userPaidAlbumService.getOne(
                new LambdaQueryWrapper<UserPaidAlbum>()
                        .eq(UserPaidAlbum::getAlbumId, albumId)
                        .eq(UserPaidAlbum::getUserId, userId)) != null;
    }

    /**
     * 查询用户购买过的指定专辑的声音的键值对
     *
     * @param albumId 专辑id
     * @param userId  用户id
     * @return 声音的键值对
     */
    @GetMapping(value = "/getUserPaidTrack/{albumId}/{userId}")
    public Map<String, Object> getUserPaidTrack(@PathVariable(value = "albumId") Long albumId,
                                                @PathVariable(value = "userId") Long userId) {
        return userPaidTrackService.getUserPaidTrack(albumId, userId);
    }

    /**
     * 查询vip详细信息
     *
     * @param id vip服务id
     * @return vip详细信息
     */
    @GetMapping(value = "/getVipServiceConfig/{id}")
    public VipServiceConfig getVipServiceConfig(@PathVariable(value = "id") Long id) {
        return vipServiceConfigService.getById(id);
    }
}