package com.atguigu.tingshu.user.api;

import com.atguigu.tingshu.common.result.Result;
import com.atguigu.tingshu.user.service.UserInfoService;
import com.atguigu.tingshu.vo.user.UserInfoVo;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Tag(name = "微信授权登录接口")
@RestController
@RequestMapping("/api/user/wxLogin")
@Slf4j
@AllArgsConstructor
public class WxLoginApiController {


    private UserInfoService userInfoService;


    /**
     * 微信登录
     *
     * @param code 微信授权码
     * @return 用户信息
     */
    @GetMapping(value = "/wxLogin/{code}")
    public Result wxLogin(@PathVariable(value = "code") String code) {
        return Result.ok(userInfoService.wxLogin(code));
    }

    /**
     * 刷新token
     *
     * @param deviceID 设备号
     * @return 新令牌
     */
    @GetMapping(value = "/getNewToken/{deviceID}")
    public Result getNewToken(@RequestParam String deviceID) {
        userInfoService.getNewToken(deviceID);
        return Result.ok();
    }

    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    @GetMapping("/getUserInfo")
    public Result<UserInfoVo> getUserInfo() {
        return Result.ok(userInfoService.getUserInfo());
    }
}
