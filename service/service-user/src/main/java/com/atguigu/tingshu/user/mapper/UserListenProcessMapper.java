package com.atguigu.tingshu.user.mapper;

import com.atguigu.tingshu.model.user.UserListenProcessTable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/***
 *  自定义 用户听书进度 Mapper 用于操作数据库
 */
@Mapper
public interface UserListenProcessMapper extends BaseMapper<UserListenProcessTable> {

}