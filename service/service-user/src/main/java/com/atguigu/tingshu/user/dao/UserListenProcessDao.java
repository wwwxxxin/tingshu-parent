package com.atguigu.tingshu.user.dao;

import com.atguigu.tingshu.model.user.UserListenProcess;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/***
 * 用户声音播放进度dao --mongodb
 */
@Repository
public interface UserListenProcessDao extends MongoRepository<UserListenProcess, String> {

}