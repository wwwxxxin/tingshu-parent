package com.atguigu.tingshu.user.service.impl;

import com.atguigu.tingshu.model.user.UserPaidAlbum;
import com.atguigu.tingshu.user.mapper.UserPaidAlbumMapper;
import com.atguigu.tingshu.user.service.UserPaidAlbumService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/***
 * 专辑购买记录 接口实现类
 */
@Service
public class UserPaidAlbumServiceImpl extends ServiceImpl<UserPaidAlbumMapper, UserPaidAlbum> implements UserPaidAlbumService {
}

