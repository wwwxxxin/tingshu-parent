package com.atguigu.tingshu.user.service.impl;

import com.atguigu.tingshu.model.user.UserPaidTrack;
import com.atguigu.tingshu.user.mapper.UserPaidAlbumMapper;
import com.atguigu.tingshu.user.mapper.UserPaidTrackMapper;
import com.atguigu.tingshu.user.service.UserPaidTrackService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 声音购买记录 接口实现类
 */
@Service
@SuppressWarnings({"unchecked", "rawtypes"})
public class UserPaidTrackServiceImpl extends ServiceImpl<UserPaidTrackMapper, UserPaidTrack> implements UserPaidTrackService {

    @Autowired
    private UserPaidAlbumMapper userPaidAlbumMapper;

    /**
     * 查询用户购买过的指定专辑的声音的键值对
     *
     * @param albumId 专辑id
     * @param userId  用户id
     * @return 声音的键值对
     */
    @Override
    public Map<String, Object> getUserPaidTrack(Long albumId, Long userId) {
        // 查询购买过的指定专辑的声音列表
        List<UserPaidTrack> userPaidTrackList =
                list(new LambdaQueryWrapper<UserPaidTrack>()
                        .eq(UserPaidTrack::getUserId, userId)
                        .eq(UserPaidTrack::getAlbumId, albumId));
        if (userPaidTrackList == null) {
            return new HashMap<>();
        }
        // 转换map返回
        return userPaidTrackList.stream().collect(Collectors.toMap(
                key -> key.getTrackId().toString(),
                value -> 1
        ));
    }
}
