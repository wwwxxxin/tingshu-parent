package com.atguigu.tingshu.user.mapper;

import com.atguigu.tingshu.model.user.UserCollectTable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/***
 *  自定义 用户收藏 Mapper 用于操作数据库
 */
@Mapper
public interface UserCollectMapper extends BaseMapper<UserCollectTable> {

}
