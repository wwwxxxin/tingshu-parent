package com.atguigu.tingshu.user.api;

import com.atguigu.tingshu.common.result.Result;
import com.atguigu.tingshu.user.service.UserListenProcessService;
import com.atguigu.tingshu.vo.user.UserListenProcessVo;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Tag(name = "用户声音播放进度管理接口")
@RestController
@AllArgsConstructor
@RequestMapping("/api/user/userListenProcess")
@SuppressWarnings({"unchecked", "rawtypes"})
public class UserListenProcessApiController {

    private UserListenProcessService userListenProcessService;

    /**
     * 查询用户声音播放进度
     *
     * @param trackId 声音id
     * @return 进度
     */
    @GetMapping(value = "/getTrackBreakSecond/{trackId}")
    public Result getTrackBreakSecond(@PathVariable(value = "trackId") Long trackId) {
        return Result.ok(userListenProcessService.getTrackBreakSecond(trackId));
    }

    /**
     * 更新用户声音播放进度
     *
     * @param userListenProcessVo 进度
     * @return 进度
     */
    @PostMapping(value = "/updateListenProcess")
    public Result updateListenProcess(@RequestBody UserListenProcessVo userListenProcessVo) {
        userListenProcessService.updateListenProcess(userListenProcessVo);
        return Result.ok();
    }
}

