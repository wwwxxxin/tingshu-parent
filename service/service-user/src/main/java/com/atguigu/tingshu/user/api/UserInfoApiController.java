package com.atguigu.tingshu.user.api;

import com.atguigu.tingshu.common.result.Result;
import com.atguigu.tingshu.user.service.UserInfoService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "用户管理接口")
@RestController
@AllArgsConstructor
@RequestMapping("/api/user/userInfo")
@SuppressWarnings({"unchecked", "rawtypes"})
public class UserInfoApiController {

    private UserInfoService userInfoService;

    /**
     * 用户收藏
     *
     * @param trackId 声音id
     * @return 结果
     */
    @GetMapping("/collect/{trackId}")
    public Result collect(@PathVariable(value = "trackId") Long trackId) {
        userInfoService.collect(trackId);
        return Result.ok();
    }

    /**
     * 用户取消收藏
     *
     * @param trackId 声音id
     * @return 结果
     */
    @GetMapping("/cancelCollect/{trackId}")
    public Result cancelCollect(@PathVariable(value = "trackId") Long trackId) {
        userInfoService.cancelCollect(trackId);
        return Result.ok();
    }

    /**
     * 查询用户是否收藏了声音
     *
     * @param trackId 声音id
     * @return 进度
     */
    @GetMapping("/isCollect/{trackId}")
    public Result isCollect(@PathVariable(value = "trackId") Long trackId) {
        return Result.ok(userInfoService.isCollect(trackId));
    }
}

