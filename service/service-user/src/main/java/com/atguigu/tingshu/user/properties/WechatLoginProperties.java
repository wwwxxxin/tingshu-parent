package com.atguigu.tingshu.user.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "wechat.login")
public class WechatLoginProperties {

    private String appId;

    private String appSecret;

}
