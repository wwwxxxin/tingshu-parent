package com.atguigu.tingshu.user.service;

import com.atguigu.tingshu.model.user.UserInfo;
import com.atguigu.tingshu.vo.user.UserInfoVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

public interface UserInfoService extends IService<UserInfo> {

    /**
     * 微信登录
     *
     * @param code 微信授权码
     * @return token
     */
    Object wxLogin(String code);


    /**
     * 刷新新令牌
     *
     * @param deviceID 设备号
     * @return 新令牌
     */
    Map getNewToken(String deviceID);


    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    UserInfoVo getUserInfo();

    /**
     * 用户收藏
     *
     * @param trackId 声音id
     */
    void collect(Long trackId);

    /**
     * 用户取消收藏
     *
     * @param trackId 声音id
     */
    void cancelCollect(Long trackId);

    /**
     * 查询用户是否收藏了声音
     *
     * @param trackId 声音id
     * @return 结果
     */
    Object isCollect(Long trackId);
}
