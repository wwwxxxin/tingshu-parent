package com.atguigu.tingshu.user.service;


import com.atguigu.tingshu.model.user.UserPaidAlbum;
import com.baomidou.mybatisplus.extension.service.IService;

/***
 * 专辑购买记录 接口
 */
public interface UserPaidAlbumService extends IService<UserPaidAlbum> {
}

