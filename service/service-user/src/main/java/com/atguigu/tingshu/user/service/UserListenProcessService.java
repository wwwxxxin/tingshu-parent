package com.atguigu.tingshu.user.service;

import com.atguigu.tingshu.vo.user.UserListenProcessVo;

public interface UserListenProcessService {

    /**
     * 查询用户声音播放进度
     *
     * @param trackId 声音id
     * @return 进度
     */
    Object getTrackBreakSecond(Long trackId);

    /**
     * 更新用户声音播放进度
     *
     * @param userListenProcessVo 进度
     */
    void updateListenProcess(UserListenProcessVo userListenProcessVo);
}