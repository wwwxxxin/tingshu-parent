package com.atguigu.tingshu.search.dao;

import com.atguigu.tingshu.model.search.SuggestIndex;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/***
 * 搜索提示词的dao层
 */
@Repository
public interface SuggestIndexDao extends ElasticsearchRepository<SuggestIndex, String> {
}
