package com.atguigu.tingshu.search.api;

import com.atguigu.tingshu.common.result.Result;
import com.atguigu.tingshu.query.search.AlbumIndexQuery;
import com.atguigu.tingshu.search.service.SearchService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Tag(name = "搜索专辑管理")
@RestController
@RequestMapping("/api/search/albumInfo")
@SuppressWarnings({"unchecked", "rawtypes"})
public class SearchApiController {

    @Autowired
    private SearchService searchService;


    /**
     * 获取首页 一级分类下 热门Top7三级分类的 热门Top6的专辑数据
     *
     * @param category1Id 一级分类ID
     * @return 专辑数据
     */
    @GetMapping(value = "/channel/{category1Id}")
    public Result channel(@PathVariable(value = "category1Id") Long category1Id) {
        return Result.ok(searchService.channel(category1Id));
    }

    /**
     * 搜索专辑
     *
     * @param albumIndexQuery 专辑搜索vo实体
     * @return 专辑数据
     */
    @PostMapping
    public Result search(@RequestBody AlbumIndexQuery albumIndexQuery) {
        return Result.ok(searchService.search(albumIndexQuery));
    }

    /**
     * 提示词补全
     *
     * @param keywords 关键字
     * @return 提示词列表
     */
    @GetMapping(value = "/completeSuggest/{keywords}")
    public Result completeSuggest(@PathVariable(value = "keywords") String keywords) {
        return Result.ok(searchService.completeSuggest(keywords));
    }

    /**
     * 查询专辑的详情信息
     *
     * @param albumId 专辑ID
     * @return 专辑详情信息
     */
    @GetMapping(value = "/{albumId}")
    public Result getAlbumInfo(@PathVariable(value = "albumId") Long albumId) {
        return Result.ok(searchService.getAlbumInfo(albumId));
    }


}

