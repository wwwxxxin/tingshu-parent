package com.atguigu.tingshu.search.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/***
 * 搜索微服务使用的线程池配置
 */
@Configuration
public class SearchThreadPoolConfig {

    /**
     * 自定义线程池
     *
     * @return 线程池对象
     */
    @Bean
    public ThreadPoolExecutor threadPoolExecutor() {
        // 初始化
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                12,    // 核心线程数
                24,               // 最大线程数
                10,               // 闲置时间
                TimeUnit.SECONDS, // 闲置时间单位
                new ArrayBlockingQueue<>(100),// 阻塞队列
                Executors.defaultThreadFactory(),     // 线程工厂对象
                new ThreadPoolExecutor.AbortPolicy());// 拒绝策略
        // 返回
        return threadPoolExecutor;
    }

}
