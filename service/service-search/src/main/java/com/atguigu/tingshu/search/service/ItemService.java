package com.atguigu.tingshu.search.service;

public interface ItemService {

    /**
     * 专辑写入es
     *
     * @param albumId 专辑id
     */
    void addAlbumFromMysqlToEs(Long albumId);

    /**
     * 专辑从es删除
     *
     * @param albumId 专辑id
     */
    void removeFromEs(Long albumId);

}
