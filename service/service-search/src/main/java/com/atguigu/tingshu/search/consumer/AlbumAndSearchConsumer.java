package com.atguigu.tingshu.search.consumer;

import com.atguigu.tingshu.search.service.ItemService;
import com.rabbitmq.client.Channel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/***
 * 专辑上架和下架消费者
 */
@Component
@Log4j2
@RequiredArgsConstructor
public class AlbumAndSearchConsumer {

    private final ItemService itemService;

    /**
     * 专辑上架 消费者
     *
     * @param channel 通道
     * @param message 消息
     */
    @SneakyThrows
    @RabbitListener(queues = "album_and_search_queue_upper") // 监听的队列
    public void albumUpperConsumer(Channel channel, Message message) {
        // 获取消息:专辑id
        byte[] body = message.getBody();
        Long albumId = Long.valueOf(new String(body));
        // 获取消息的属性
        MessageProperties messageProperties = message.getMessageProperties();
        // 获取消息唯一编号
        long deliveryTag = messageProperties.getDeliveryTag();
        try {
            // 上架
            itemService.addAlbumFromMysqlToEs(albumId);
            // 确认消息
            channel.basicAck(deliveryTag, false);
        } catch (Exception e) {
            // 判断消息是否重发过
            if (messageProperties.getRedelivered()) {
                // 重发过 记录日志丢弃
                log.error("接受专辑上架消息消费失败,专辑的id为:" + albumId
                        + ",失败的原因为:" + e.getMessage());
                channel.basicReject(deliveryTag, false);
            } else {
                // 第一次 就放回去再来一次
                channel.basicReject(deliveryTag, true);
            }
        }
    }

    /**
     * 专辑下架 消费者
     *
     * @param channel 通道
     * @param message 消息
     */
    @SneakyThrows
    @RabbitListener(queues = "album_and_search_queue_down") // 监听的队列
    public void albumDownConsumer(Channel channel, Message message) {
        // 获取消息:专辑id
        byte[] body = message.getBody();
        Long albumId = Long.valueOf(new String(body));
        // 获取消息的属性
        MessageProperties messageProperties = message.getMessageProperties();
        // 获取消息唯一编号
        long deliveryTag = messageProperties.getDeliveryTag();
        try {
            // 下架
            itemService.removeFromEs(albumId);
            // 确认消息
            channel.basicAck(deliveryTag, false);
        } catch (Exception e) {
            // 判断消息是否被重发过
            if (messageProperties.getRedelivered()) {
                //  重发过 记录日志丢弃
                log.error("接受专辑下架消息消费失败,专辑的id为:" + albumId
                        + ",失败的原因为:" + e.getMessage());
                channel.basicReject(deliveryTag, false);
            } else {
                // 第一次 就放回去再来一次
                channel.basicReject(deliveryTag, true);
            }
        }
    }

}
