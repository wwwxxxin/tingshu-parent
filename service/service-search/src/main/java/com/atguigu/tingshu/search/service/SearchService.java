package com.atguigu.tingshu.search.service;

import com.atguigu.tingshu.query.search.AlbumIndexQuery;

public interface SearchService {


    /**
     * 获取首页 一级分类下 热门TOP7三级分类的 热门Top6的专辑数据
     *
     * @param category1Id 一级分类ID
     * @return 专辑数据
     */
    Object channel(Long category1Id);

    /**
     * 搜索专辑
     *
     * @param albumIndexQuery 专辑搜索vo实体
     * @return 专辑数据
     */
    Object search(AlbumIndexQuery albumIndexQuery);

    /**
     * 提示词补全
     *
     * @param keywords 关键字
     * @return 提示词列表
     */
    Object completeSuggest(String keywords);

    /**
     * 查询专辑的详情信息
     *
     * @param albumId 专辑ID
     * @return 专辑详情信息
     */
    Object getAlbumInfo(Long albumId);
}
