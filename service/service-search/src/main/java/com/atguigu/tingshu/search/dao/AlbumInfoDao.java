package com.atguigu.tingshu.search.dao;

import com.atguigu.tingshu.model.search.AlbumInfoIndex;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/***
 * 专辑es实体的dao层
 */
@Repository
public interface AlbumInfoDao extends ElasticsearchRepository<AlbumInfoIndex, Long> {
}
