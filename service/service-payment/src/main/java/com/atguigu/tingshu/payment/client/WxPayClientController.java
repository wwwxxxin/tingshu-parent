package com.atguigu.tingshu.payment.client;

import com.atguigu.tingshu.payment.service.WxPayService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * 支付微服务 内部调用接口
 */
@RestController
@AllArgsConstructor
@RequestMapping(value = "/client/payment/wxPay")
public class WxPayClientController {

    private WxPayService wxPayService;

    /**
     * 向微信支付系统发起请求，获取支付地址信息
     *
     * @param orderNo 订单号
     * @param money   金额
     * @param desc    描述
     * @return 支付地址信息
     */
    @GetMapping(value = "/getPayInfoFromWx/{orderNo}/{money}/{desc}")
    public String getPayInfoFromWx(@PathVariable(value = "orderNo") String orderNo,
                                   @PathVariable(value = "money") String money,
                                   @PathVariable(value = "desc") String desc) {
        return wxPayService.getPayInfoFromWx(orderNo, money, desc);
    }

    /**
     * 获取订单支付结果 (主动查询)
     *
     * @param orderNo 订单号
     * @return  支付结果
     */
    @GetMapping(value = "/getOrderPayResult/{orderNo}")
    public String getOrderPayResult(@PathVariable(value = "orderNo") String orderNo) {
        return wxPayService.getOrderPayResult(orderNo);
    }
}
