package com.atguigu.tingshu.payment.service;

import java.util.Map;

public interface WxPayService {

    /**
     * 向微信支付系统发起请求，获取支付地址信息
     *
     * @param orderNo 订单号
     * @param money   金额
     * @param desc    描述
     * @return 支付地址信息
     */
    String getPayInfoFromWx(String orderNo, String money, String desc);

    /**
     * 获取订单支付结果
     *
     * @param orderNo 订单号
     * @return 支付结果
     */
    String getOrderPayResult(String orderNo);

    /**
     * 根据微信通知的支付结果，修改支付的流水数据
     *
     * @param result 支付结果
     */
    void updatePaymentInfo(Map<String, String> result);
}
