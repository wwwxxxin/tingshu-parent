package com.atguigu.tingshu.payment.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/***
 * 支付微服务 feign接口
 */
@FeignClient(name = "service-payment", path = "/client/payment/wxPay", contextId = "paymentFeignClient")
public interface PaymentFeignClient {

    /**
     * 向微信支付系统发起请求，获取支付地址信息
     *
     * @param orderNo 订单号
     * @param money   金额
     * @param desc    描述
     * @return 支付地址信息
     */
    @GetMapping(value = "/getPayInfoFromWx/{orderNo}/{money}/{desc}")
    public String getPayInfoFromWx(@PathVariable(value = "orderNo") String orderNo,
                                   @PathVariable(value = "money") String money,
                                   @PathVariable(value = "desc") String desc) ;
}