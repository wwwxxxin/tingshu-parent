package com.atguigu.tingshu.album.client;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.tingshu.model.album.TrackInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/***
 * 声音 feign接口
 */
@FeignClient(name = "service-album", path = "/client/track/trackInfo", contextId = "trackClientFeign")
public interface TrackClientFeign {


    /**
     * 查询声音是否存在
     *
     * @param trackId 声音id
     * @return 是否存在
     */
    @GetMapping(value = "/getTrackInfo/{trackId}")
    public Boolean getTrackInfo(@PathVariable(value = "trackId") Long trackId);

    /**
     * 查询即将订单确认的声音信息
     *
     * @param trackId 声音id
     * @return 声音信息
     */
    @GetMapping(value = "/getTrackListAndPrice/{trackId}")
    public JSONObject getTrackListAndPrice(@PathVariable(value = "trackId") Long trackId);

    /**
     * 查询声音详情信息
     *
     * @param trackId 声音id
     * @return 声音信息
     */
    @GetMapping(value = "/getTrackInfoDetail/{trackId}")
    public TrackInfo getTrackInfoDetail(@PathVariable(value = "trackId") Long trackId);
}