package com.atguigu.tingshu.order.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

/**
 * 产品列表API接口
 */
@FeignClient(value = "service-order", path = "/client/order/orderInfo", contextId = "orderInfoFeignClient")
public interface OrderInfoFeignClient {

    /**
     * 查询用户准备购买的声音列表
     *
     * @param userId 用户id
     * @return 声音列表
     */
    @GetMapping(value = "/getUserTrackOrderInfo/{userId}")
    public Map<String, Object> getUserTrackOrderInfo(@PathVariable(value = "userId") Long userId);

}