package com.atguigu.tingshu.user.client;


import com.atguigu.tingshu.model.user.UserInfo;
import com.atguigu.tingshu.model.user.VipServiceConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

/***
 * 用户微服务 feign接口
 */
@FeignClient(name = "service-user", path = "/client/user/userInfo", contextId = "userClientFeign")
public interface UserClientFeign {

    /**
     * 查询用户详细信息
     *
     * @param userId 用户id
     * @return 用户详细信息
     */
    @GetMapping(value = "/getUserInfo/{userId}")
    public UserInfo getUserInfo(@PathVariable(value = "userId") Long userId);


    /**
     * 查询用户的专辑购买记录
     *
     * @param albumId 专辑id
     * @param userId  用户id
     * @return 是否购买
     */
    @GetMapping(value = "/getUserPaidAlbum/{albumId}/{userId}")
    public Boolean getUserPaidAlbum(@PathVariable(value = "albumId") Long albumId,
                                    @PathVariable(value = "userId") Long userId);

    /**
     * 查询用户购买过的指定专辑的声音的键值对
     *
     * @param albumId 专辑id
     * @param userId  用户id
     * @return 声音的键值对
     */
    @GetMapping(value = "/getUserPaidTrack/{albumId}/{userId}")
    public Map<String, Object> getUserPaidTrack(@PathVariable(value = "albumId") Long albumId,
                                                @PathVariable(value = "userId") Long userId);

    /**
     * 查询vip详细信息
     *
     * @param id vip服务id
     * @return vip详细信息
     */
    @GetMapping(value = "/getVipServiceConfig/{id}")
    public VipServiceConfig getVipServiceConfig(@PathVariable(value = "id") Long id);
}