package com.atguigu.tingshu.common.cache;


import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/***
 * 自定义切面类：用于缓存方法
 */
@Aspect
@Component
@AllArgsConstructor
public class GuiguCacheAspect {

    private RedisTemplate redisTemplate;

    private RedissonClient redissonClient;

    /**
     * 切面缓存增强方法
     *
     * @param point 切入点
     * @return 返回结果
     */
    @SneakyThrows
    @Around("@annotation(com.atguigu.tingshu.common.cache.GuiguCache)")
    public Object guiguCacheAspectMethod(ProceedingJoinPoint point) {

        // 返回结果初始化
        Object result = new Object();

        // 获取方法参数
        Object[] args = point.getArgs();

        // 获取方法签名
        MethodSignature signature = (MethodSignature) point.getSignature();
        // 获取方法返回类型
        Class returnType = signature.getReturnType();
        // 获取目标方法
        Method method = signature.getMethod();

        // 获取方法的注解 GuiguCache
        GuiguCache guiguCache = method.getAnnotation(GuiguCache.class);
        // 获取前缀属性
        String prefix = guiguCache.prefix();

        //  生成缓存的key = 前缀 + 方法参数的字符串
        String key = prefix + Arrays.asList(args).toString();

        // 先到redis中获取数据
        String redisResult = (String) redisTemplate.opsForValue().get(key);
        if (StringUtils.isNotEmpty(redisResult)) {
            // redis中有数据：直接返回
            return JSONObject.parseObject(redisResult, returnType);
        }

        // 加锁
        RLock lock = redissonClient.getLock(key + "lock");
        if (lock.tryLock(100, 100, TimeUnit.SECONDS)) {
            try {
                // redis中没有数据：执行方法
                result = point.proceed(args);

                // 方法执行后: 写redis
                if (result == null) {
                    // 实例化一个空对象: 获取构造器 实例化
                    result = returnType.getConstructor().newInstance();
                    // 数据库也没有:存储到redis中5分钟过期
                    redisTemplate.opsForValue().set(key, JSONObject.toJSONString(result), 5, TimeUnit.MINUTES);
                } else {
                    // 数据库有: 存储到redis中1天过期
                    redisTemplate.opsForValue().set(key, JSONObject.toJSONString(result), 1, TimeUnit.DAYS);
                }
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // 释放锁
                lock.unlock();
            }
        }
        // 返回
        return result;
    }

}
